import 'package:flutter/material.dart';
import 'package:prueba_peliculas/styles/styles/theme.dart';

class CustomTheme implements ThemeStructure {
  @override
  final colors = const ColorsTheme(
      darkBlue: Color(0xffde2e03),
      lightBlue: Color(0xffff8000),
      white: Color(0xffffffff),
      black: Color(0xff000000));
}
