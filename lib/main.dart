import 'package:flutter/material.dart';
import 'package:prueba_peliculas/common/models/users_repository.dart';
import 'package:prueba_peliculas/src/login/screen/login_screen.dart';
import 'package:prueba_peliculas/src/home/movie_list.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({super.key});

  final UsersRepository usersRepository = UsersRepository();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
        textSelectionTheme:
            const TextSelectionThemeData(cursorColor: Colors.black),
      ),
      home: FutureBuilder(future: usersRepository.isLogin(), builder: (context, snapshot) {
          if(!snapshot.hasData || snapshot.data == false){
            return LoginScreen();
          }else{
            return MovieList();
          }
      },),
    );
  }
}
