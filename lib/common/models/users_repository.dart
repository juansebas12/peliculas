import 'dart:convert';

import 'package:prueba_peliculas/common/models/user_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UsersRepository{

  late UsersData usersData = UsersData(users: []);

  createUser(String name, String correo, String password) async{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    usersData.users?.add(User(nombre: name,correo: correo,password: password));
    prefs.setString('users', jsonEncode(usersData.toJson()));
  }

  Future<User?> getUser(String correo, String password) async{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final users = prefs.getString('users');
    if(users != null){
      usersData = UsersData.fromJson(jsonDecode(users));
      for (var i = 0; i < usersData.users!.length; i++) {
        if(usersData.users?[i].correo == correo && usersData.users?[i].password == password){
          prefs.setBool('IsLogin', true);
          return usersData.users?[i];
        } 
      }
    }
    return null;
  }

  Future<bool> isLogin() async{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isLogin = prefs.getBool('IsLogin') ?? false;
    return isLogin;
  }

  close() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('IsLogin');
  }

}

