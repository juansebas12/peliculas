class UsersData {
  late List<User>? users;

  UsersData({
    this.users,
  });

  UsersData.fromJson(Map<String, dynamic> json)
      : users = (json['users'] as List?)?.map((dynamic e) => User.fromJson(e as Map<String, dynamic>)).toList();

  Map<String, dynamic> toJson() => {'users': users?.map((e) => e.toJson()).toList()};
}

class User {
  late String? nombre;
  late String? correo;
  late String? password;

  User({
    this.nombre,
    this.correo,
    this.password,
  });

  User.fromJson(Map<String, dynamic> json)
      : nombre = json['nombre'] as String?,
        correo = json['correo'] as String?,
        password = json['password'] as String?;

  Map<String, dynamic> toJson() => {'nombre': nombre, 'correo': correo, 'password': password};
}
