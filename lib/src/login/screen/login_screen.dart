import 'package:flutter/material.dart';
import 'package:prueba_peliculas/common/models/user_model.dart';
import 'package:prueba_peliculas/common/models/users_repository.dart';
import 'package:prueba_peliculas/common/routes/redirect_service.dart';
import 'package:prueba_peliculas/src/register/register_screen.dart';
import 'package:prueba_peliculas/styles/custom_button.dart';
import 'package:prueba_peliculas/src/home/movie_list.dart';
import 'package:prueba_peliculas/styles/styles/custom_theme.dart';
import 'package:prueba_peliculas/styles/styles/text_field_style.dart';

class LoginScreen extends StatelessWidget {
  LoginScreen({super.key});

  final User user = User();
   final UsersRepository usersRepository = UsersRepository();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomRight,
          colors: [
            CustomTheme().colors.darkBlue,
            CustomTheme().colors.lightBlue,
            CustomTheme().colors.white
          ],
        )),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 60.0, left: 8.0, right: 8.0),
              child: Text(
                "Iniciar Sesión",
                style: TextStyle(
                  color: CustomTheme().colors.black,
                  fontWeight: FontWeight.w700,
                  fontSize: 18.0,
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 60.0, left: 32.0, right: 32.0),
              child: Form(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  
                  TextFormField(
                    style: TextFieldStyle.textStyle(
                        colorText: CustomTheme().colors.black),
                    onChanged: (value) {
                      user.correo = value;
                    },
                    maxLines: 1,
                    decoration: TextFieldStyle.general(labelText: "Correo"),
                    keyboardType: TextInputType.emailAddress,
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  TextFormField(
                    style: TextFieldStyle.textStyle(colorText: CustomTheme().colors.black),
                    onChanged: (value) {
                      user.password = value;
                    },
                    decoration: TextFieldStyle.general(labelText: "Contraseña"),
                    obscureText: true,
                  ),
                  const SizedBox(
                    height: 40.0,
                  ),
                  CustomButton.button(
                    title: const Text("Ingresar"),
                    onPressed: () async{
                      User? userResult = await usersRepository.getUser(user.correo ?? "", user.password ?? "");
                      if(userResult != null){
                        // ignore: use_build_context_synchronously
                        RedirectService.replaceAllPages(context, MovieList());
                      }
                    },
                  ),
                ],
              )),
            ),
            const SizedBox(
              height: 30.0,
            ),
            TextButton(
                onPressed: () {
                  RedirectService.goTo(context, RegisterScreen());
                },
                child: Text(
                  "Registro",
                  style: TextStyle(
                    decoration: TextDecoration.underline, decorationColor: CustomTheme().colors.darkBlue,
                    color: CustomTheme().colors.darkBlue,
                    fontWeight: FontWeight.w700,
                    fontSize: 14,
                  ),
                )),
          ],
        ),
      ),
    );
  }
}
