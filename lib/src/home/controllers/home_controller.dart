import 'package:prueba_peliculas/src/home/providers/home_providers.dart';
import 'package:prueba_peliculas/src/movie/models/movie_model.dart';
import 'package:rxdart/rxdart.dart';

class HomeController{

  final provider = HomeProvider();

  BehaviorSubject<Movies?> moviesCollection = BehaviorSubject<Movies?>();
  Movies? movies;
  Stream<Movies?> get moviesStream => moviesCollection.stream;

  getMovies() async{
    final response = await provider.getMoviesApi();
    movies = Movies.fromJson(response);
    moviesCollection.sink.add(movies);
  }
}