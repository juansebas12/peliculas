import 'package:flutter/material.dart';
import 'package:prueba_peliculas/common/models/users_repository.dart';
import 'package:prueba_peliculas/common/routes/redirect_service.dart';
import 'package:prueba_peliculas/src/login/screen/login_screen.dart';
import 'package:prueba_peliculas/src/gallery/gallery_screen.dart';
import 'package:prueba_peliculas/src/home/controllers/home_controller.dart';
import 'package:prueba_peliculas/src/movie/home.dart';
import 'package:prueba_peliculas/src/movie/models/movie_model.dart';
import 'package:prueba_peliculas/styles/styles/custom_theme.dart';

class MovieList extends StatelessWidget {

  final HomeController homeController = HomeController();
  final UsersRepository usersRepository = UsersRepository();

  MovieList({super.key}){
    homeController.getMovies();
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: CustomTheme().colors.lightBlue,
        leading: const SizedBox.shrink(),
        title: Text(
          "Peliculas",
          style: TextStyle(
            color: CustomTheme().colors.white,
            fontWeight: FontWeight.w700,
            fontSize: 18.0,
          ),
        ),
      ),
      body: StreamBuilder<Movies?>(
        stream: homeController.moviesStream,
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          return SizedBox(
            height: 600.0,
            child: ListView.separated(
              scrollDirection: Axis.horizontal,
              padding: const EdgeInsets.all(10.0),
              itemCount: snapshot.data?.results?.length ?? 0,
              itemBuilder: (context, index) {
                return cardMovie(context, size, snapshot.data?.results?[index]);
              },
              separatorBuilder: (context, index) {
                return const SizedBox(
                  width: 10.0,
                );
              },
            ),
          );
        }
      ),
      bottomNavigationBar: Container(
        width: double.infinity,
        height: 60.0,
        color: CustomTheme().colors.lightBlue,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            InkWell(
              onTap: () {
                RedirectService.goTo(context, const GalleryScreen());
              },
              child: Row(
                children: [
                  Icon(Icons.photo_library, color: CustomTheme().colors.white),
                  const SizedBox(width: 10.0,),
                  Text(
                    "Video",
                    style: TextStyle(
                      color: CustomTheme().colors.white,
                      fontWeight: FontWeight.w700,
                      fontSize: 18.0,
                    ),
                  ),
                ],
              ),
            ),
            InkWell(
              onTap: () {
                usersRepository.close();
                RedirectService.replaceAllPages(context, LoginScreen());
              },
              child:  Row(
                children: [
                  Icon(Icons.login, color: CustomTheme().colors.white),
                  const SizedBox(width: 10.0,),
                  Text(
                    "Salir",
                    style: TextStyle(
                      color: CustomTheme().colors.white,
                      fontWeight: FontWeight.w700,
                      fontSize: 18.0,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  cardMovie(BuildContext context,Size size, Movie? movie) {
    return InkWell(
      onTap: () {
        RedirectService.goTo(context, MovieDetails(movie: movie,));
      },
      child: Container(
        padding: const EdgeInsets.all(8.0),
        width: 300.0,
        height: 500.0,
        decoration: BoxDecoration(
          border: Border.all(color: CustomTheme().colors.darkBlue, width: 1.0),
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: Column(
          children: [
            Image.network(
                'https://image.tmdb.org/t/p/w400${movie?.posterPath ?? ""}',
                width: size.width * 0.8),
            const SizedBox(
              height: 10.0,
            ),
            Text(
              "${movie?.title}",
              style: TextStyle(
                color: CustomTheme().colors.darkBlue,
                fontWeight: FontWeight.w700,
                fontSize: 18.0,
              ),
            ),
            const SizedBox(
              height: 8.0,
            ),
            Text(
              "${movie?.overview}",
              maxLines: 3,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color: CustomTheme().colors.black,
                fontWeight: FontWeight.w500,
                fontSize: 14.0,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
