import 'dart:convert';

import 'package:http/http.dart';
import 'package:http/http.dart' as http;

class MovieProvider{
  getMovieDetailsByID(String movieId) async {
    Response httpResponse = await http.get(
        Uri.parse(
            "https://api.themoviedb.org/3/movie/$movieId?language=en-US"),
        headers: {
          'Authorization':
              "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI3ZmM5ZTUwZGI0NmE0OTZjODU1MThlYWNmZDEzMTk0ZSIsInN1YiI6IjYxMjFiZDhlZDRiOWQ5MDA3YzUyMGJkOSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.JGLlSYMUO_MNTYYxAmQXjE7-y0dphBrsB1Iy8t3__94"
        });
    if (httpResponse.statusCode == 200) {
      return json.decode(utf8.decode(httpResponse.bodyBytes));
    }
    return null;
  }
}