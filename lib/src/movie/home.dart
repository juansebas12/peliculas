import 'package:flutter/material.dart';
import 'package:prueba_peliculas/common/routes/redirect_service.dart';
import 'package:prueba_peliculas/src/movie/controllers/movie_controller.dart';
import 'package:prueba_peliculas/src/movie/models/movie_model.dart';
import 'package:prueba_peliculas/styles/styles/custom_theme.dart';

// ignore: must_be_immutable
class MovieDetails extends StatelessWidget {

  final Movie? movie;

  MovieController movieController = MovieController();

  MovieDetails({super.key, required this.movie}){
    movieController.getMovieById("${movie?.id ?? ""}");
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: CustomTheme().colors.lightBlue,
        leading: InkWell(
          onTap: () {
            RedirectService.pop(context);
          },
          child: Icon(Icons.arrow_back, color: CustomTheme().colors.white,),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 16.0, right: 16.0, top: 10.0, bottom: 20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.network(
                'https://image.tmdb.org/t/p/w400${movie?.backdropPath?? ""}',
                width: size.width * 0.9),
                const SizedBox(
              height: 10.0,
            ),
                Text(
              "${movie?.title}",
              style: TextStyle(
                color: CustomTheme().colors.darkBlue,
                fontWeight: FontWeight.w700,
                fontSize: 18.0,
              ),
            ),
            const SizedBox(
              height: 10.0,
            ),
            Text(
              "${movie?.overview}",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: CustomTheme().colors.black,
                fontWeight: FontWeight.w500,
                fontSize: 16.0,
              ),
            ),
            const SizedBox(
              height: 8.0,
            ),
            Text(
              "Votación: ${movie?.voteAverage}",
              style: TextStyle(
                color: CustomTheme().colors.black,
                fontWeight: FontWeight.w500,
                fontSize: 16.0,
              ),
            ),
            const SizedBox(
              height: 8.0,
            ),
            Text(
              "Fecha: ${movie?.releaseDate}",
              style: TextStyle(
                color: CustomTheme().colors.black,
                fontWeight: FontWeight.w500,
                fontSize: 16.0,
              ),
            ),
            const SizedBox(
              height: 8.0,
            ),
            Text(
              "Popularidad: ${movie?.popularity}",
              style: TextStyle(
                color: CustomTheme().colors.black,
                fontWeight: FontWeight.w500,
                fontSize: 16.0,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
