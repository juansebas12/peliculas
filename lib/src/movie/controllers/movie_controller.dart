import 'package:prueba_peliculas/src/movie/models/movie_details_model.dart';
import 'package:prueba_peliculas/src/movie/providers/movie_provider.dart';
import 'package:rxdart/rxdart.dart';

class MovieController {
  final provider = MovieProvider();

  BehaviorSubject<MovieDetailModel?> movieDetailCollection = BehaviorSubject<MovieDetailModel?>();
  MovieDetailModel? movieDetail;
  Stream<MovieDetailModel?> get movieDetailStream => movieDetailCollection.stream;

  getMovieById(String movieId) async {
    final response = await provider.getMovieDetailsByID(movieId);
    movieDetail = MovieDetailModel.fromJson(response);
    movieDetailCollection.sink.add(movieDetail);
  }
}
