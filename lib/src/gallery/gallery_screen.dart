import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:prueba_peliculas/common/routes/redirect_service.dart';
import 'package:prueba_peliculas/styles/custom_button.dart';
import 'package:prueba_peliculas/styles/styles/custom_theme.dart';

class GalleryScreen extends StatefulWidget {
  const GalleryScreen({super.key});

  @override
  State<GalleryScreen> createState() => _GalleryScreenState();
}

class _GalleryScreenState extends State<GalleryScreen> {
  File? imageGallery;

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(
        backgroundColor: CustomTheme().colors.lightBlue,
        leading: InkWell(
          onTap: () {
            RedirectService.pop(context);
          },
          child: Icon(
            Icons.arrow_back,
            color: CustomTheme().colors.white,
          ),
        ),
        title: Text(
          "Video",
          style: TextStyle(
            color: CustomTheme().colors.white,
            fontWeight: FontWeight.w700,
            fontSize: 18.0,
          ),
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: CustomButton.button(
              title: const Text("Seleccionar imagen"),
              onPressed: () {
                selectImage();
              },
            ),
          ),
          if(imageGallery != null)...[
            Container(
            padding: const EdgeInsets.all(10.0),
            height: 500.0,
            width: 500.0,
            decoration: BoxDecoration(
               image: DecorationImage(fit: BoxFit.contain, image: FileImage(imageGallery!)),
            ),
          ),
          ]
          
        ],
      ),
    );
  }

  selectImage() async {
    final ImagePicker picker = ImagePicker();
    final image = await picker.pickVideo(source: ImageSource.camera);
    if (image != null) {
      setState(() {
        imageGallery = File(image.path);
      });
    }
  }
}