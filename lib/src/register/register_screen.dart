import 'package:flutter/material.dart';
import 'package:prueba_peliculas/common/models/user_model.dart';
import 'package:prueba_peliculas/common/models/users_repository.dart';
import 'package:prueba_peliculas/common/routes/redirect_service.dart';
import 'package:prueba_peliculas/src/home/movie_list.dart';
import 'package:prueba_peliculas/src/login/screen/login_screen.dart';
import 'package:prueba_peliculas/styles/custom_button.dart';
import 'package:prueba_peliculas/styles/styles/custom_theme.dart';
import 'package:prueba_peliculas/styles/styles/text_field_style.dart';

class RegisterScreen extends StatelessWidget {

  final User user = User();

  final UsersRepository usersRepository = UsersRepository();

  RegisterScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomRight,
          colors: [
            CustomTheme().colors.darkBlue,
            CustomTheme().colors.lightBlue,
            CustomTheme().colors.white
          ],
        )),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 60.0, left: 18.0, right: 8.0),
              child: Row(
                children: [
                  InkWell(
                    onTap: () {
                      RedirectService.pop(context);
                    },
                    child: const Icon(
                      Icons.arrow_back,
                      color: Colors.black,
                    ),
                  ),
                  SizedBox(
                    width: size.width * 0.8,
                    child: Text(
                      "Registro",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: CustomTheme().colors.black,
                        fontWeight: FontWeight.w700,
                        fontSize: 18.0,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 60.0, left: 32.0, right: 32.0),
              child: Form(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  TextFormField(
                    onChanged: (value) {
                      user.nombre = value;
                    },
                    decoration: TextFieldStyle.general(labelText: "Nombre"),
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  TextFormField(
                    onChanged: (value) {
                      user.correo = value;
                    },
                    decoration: TextFieldStyle.general(labelText: "Correo"),
                    keyboardType: TextInputType.emailAddress,
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  TextFormField(
                    onChanged: (value) {
                      user.password = value;
                    },
                    decoration: TextFieldStyle.general(labelText: "Contraseña"),
                    obscureText: true,
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  const SizedBox(
                    height: 40.0,
                  ),
                  CustomButton.button(
                      title: const Text("Regístrate"), onPressed: () {
                        usersRepository.createUser(user.nombre ?? "", user.correo ?? "", user.password ?? "");
                        usersRepository.getUser(user.correo ?? "", user.password ?? "");
                        RedirectService.replaceAllPages(context, MovieList());
                      }),
                ],
              )),
            ),
            const SizedBox(
              height: 30.0,
            ),
            TextButton(
                onPressed: () {
                  RedirectService.goTo(context, LoginScreen());
                },
                child: Text(
                  "Iniciar sesión",
                  style: TextStyle(
                    decoration: TextDecoration.underline,
                    decorationColor: CustomTheme().colors.darkBlue,
                    color: CustomTheme().colors.darkBlue,
                    fontWeight: FontWeight.w700,
                    fontSize: 14,
                  ),
                )),
          ],
        ),
      ),
    );
  }
}
